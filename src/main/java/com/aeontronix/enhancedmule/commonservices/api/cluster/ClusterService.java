package com.aeontronix.enhancedmule.commonservices.api.cluster;

import com.aeontronix.commons.StringUtils;
import com.aeontronix.enhancedmule.commonservices.internal.CircuitBreakerPersistedState;
import com.aeontronix.enhancedmule.commonservices.internal.EMCommonServicesConfiguration;
import com.hazelcast.config.Config;
import com.hazelcast.config.JoinConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.crdt.pncounter.PNCounter;
import com.hazelcast.replicatedmap.ReplicatedMap;

import java.time.Duration;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class ClusterService {
    public static final String CIRCUITBREAKER = "circuitbreaker";
    private static ClusterService instance;
    private final HazelcastInstance hz;
    private final ReplicatedMap<String, CircuitBreakerPersistedState> circuitBreakerState;
    private final HashMap<String, PNCounter> circuitBreakerErrorCount = new HashMap<>();
    private final HashMap<String, PNCounter> circuitBreakerOperationsCount = new HashMap<>();

    public static synchronized ClusterService getInstance() {
        if (instance == null) {
            throw new IllegalStateException("Common Service Cluster hasn't been configured");
        }
        return instance;
    }

    public ClusterService(EMCommonServicesConfiguration config) {
        Config hc = new Config();
        hc.getNetworkConfig().setPublicAddress("0.0.0.0")
                .setPort(config.getPort()).setPortAutoIncrement(true);
        JoinConfig join = hc.getNetworkConfig().getJoin();
        join.getAwsConfig().setEnabled(false);
        final String memberAddresses = config.getMemberAddresses();
        if (StringUtils.isNotBlank(memberAddresses)) {
            final List<String> memberAddressesArray = Arrays.stream(memberAddresses.split(","))
                    .map(s -> s.contains(":") ? s : s + ":" + config.getPort()).collect(Collectors.toList());
            join.getTcpIpConfig().setEnabled(true).setMembers(memberAddressesArray);
        }
        hz = Hazelcast.newHazelcastInstance(hc);
        circuitBreakerState = hz.getReplicatedMap(CIRCUITBREAKER);
    }

    public HazelcastInstance getHazelcastInstance() {
        return hz;
    }

    public static synchronized void launch(EMCommonServicesConfiguration config) {
        if (instance != null) {
            throw new IllegalStateException("Only one enhanced mule common services config must be set in application");
        }
        instance = new ClusterService(config);
    }

    public void circuitBreakerSchedule(Runnable runnable, String delayDuration) {
        hz.getScheduledExecutorService("cbreaker")
                .schedule(runnable, Duration.parse(delayDuration).toMillis(), TimeUnit.MILLISECONDS);
    }

    public ReplicatedMap<String, CircuitBreakerPersistedState> getCircuitBreakerState() {
        return circuitBreakerState;
    }

    public HashMap<String, PNCounter> getCircuitBreakerErrorCounter() {
        return circuitBreakerErrorCount;
    }

    public HashMap<String, PNCounter> getCircuitBreakerOperationsCount() {
        return circuitBreakerOperationsCount;
    }

    public void shutdown() {
        hz.shutdown();
        instance = null;
    }
}
