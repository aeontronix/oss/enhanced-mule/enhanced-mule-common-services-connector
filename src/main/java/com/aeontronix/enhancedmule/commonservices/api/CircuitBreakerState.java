package com.aeontronix.enhancedmule.commonservices.api;

public enum CircuitBreakerState {
    OPEN, OPEN_WITH_ERROR, CLOSED, HALF_OPEN
}
